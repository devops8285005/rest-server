package storage

import (
	"database/sql"
	"fmt"
	"rest_server/config"

	_ "github.com/lib/pq"
)

type Entity struct {
	ID        int
	URL       string
	SHORT_URL string
}

func New(cfg *config.DatabaseConfig) (*Storage, error) {
	driver := "postgres"
	connInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.DbName)

	//TODO: make it possible to use not only postgres

	db, err := sql.Open(driver, connInfo)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	if err := EnsureTableExists(db); err != nil {
		return nil, err
	}

	return &Storage{db: db}, nil
}

func EnsureTableExists(db *sql.DB) error {
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS urls (
			id SERIAL PRIMARY KEY,
			url TEXT NOT NULL,
			short_url TEXT NOT NULL UNIQUE
		);
	`)
	return err
}
